export interface CityDataQuery {
  cityId: number;
  cityName: string;
  cityDescription: string;
  cityCoverImg: string;
}
