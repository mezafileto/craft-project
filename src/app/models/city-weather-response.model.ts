export interface CityWeatherResponse {
  temperatureC: number;
  temperatureF: number;
  forecast: string;
  weatherDate: Date;
}
