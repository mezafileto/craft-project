import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CityInformationComponent } from './components/city-information/city-information.component';
import { CityWeatherComponent } from './components/city-weather/city-weather.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [CityInformationComponent, CityWeatherComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ], exports:[CityInformationComponent]
})
export class CityViewerModule { }
