import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '@env/environment';
import { Observable } from 'rxjs';

import {CityDataQuery, CityWeatherResponse } from '@craft/models';

@Injectable({
  providedIn: 'root'
})
export class CityDataService {

  private cityDataBaseUrl = `${environment.webUrl}`;
  constructor(private http: HttpClient) {}

  getCityData(): Observable<Array<CityDataQuery>> {
    return this.http.get<Array<CityDataQuery>>(`${this.cityDataBaseUrl}/city/getall`, { });
  }

  getCityWeather(citySelected: number): Observable<Array<CityWeatherResponse>> {
    return this.http.get<Array<CityWeatherResponse>>(`${this.cityDataBaseUrl}/city/GetWeatherByCityId/${citySelected}`, { });
  }
}
