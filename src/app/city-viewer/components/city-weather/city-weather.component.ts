import { Component, Input, OnInit } from '@angular/core';
import { CityDataService } from '@craft/city-viewer/services/https';
import { CityWeatherResponse } from '@craft/models';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.css']
})
export class CityWeatherComponent implements OnInit {

  @Input() citySelected = 0; // decorate the property with @Input()
  isLoading = false;
  citiesWeatherRequestList: Array<CityWeatherResponse>;

  constructor(
     private cityDataService: CityDataService,
    ) { }

  ngOnInit(): void {
  }

  ngOnChanges(val){
    if(this.citySelected != 0)
    {
      this.citiesWeatherRequestList=[];
      this.isLoading = true;
      this.fetchCitiesWeather(this.citySelected);
    }
  }

  fetchCitiesWeather(citySelected: number): void {
  this.cityDataService
  .getCityWeather(citySelected)
  .subscribe((response) => {
    this.isLoading = false;
    this.citiesWeatherRequestList = response;
  });
}

}
