import { Injectable } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { CityDataService } from '@craft/city-viewer/services/https';
import { CityDataQuery } from '@craft/models';
import { CompileShallowModuleMetadata } from '@angular/compiler';

@Injectable({
  providedIn: 'root',
})

@Component({
  selector: 'app-city-information',
  templateUrl: './city-information.component.html',
  styleUrls: ['./city-information.component.css']
})
export class CityInformationComponent implements OnInit {

  form: FormGroup;
  citiesRequestList: Array<CityDataQuery>;
  cityId = 1;
  cityDescription = "";

  readonly formConst = {
    cityList: 'cityList',
  };

  constructor(
    private cityDataService: CityDataService,
    private formBuilder: FormBuilder,
    ) {}

  ngOnInit(): void {
   this.initForm();
   this.fetchCities();
  }

  fetchCities(): void {
    this.cityDataService
    .getCityData()
    .subscribe((response) => {
      this.citiesRequestList = response;
      this.setDescriptionCity();
    });
  }

  private initForm(): void {
    this.form = new FormGroup({
      [this.formConst.cityList]: new FormControl(1, [Validators.required]),
    });
  }

  onSelectCityChange(event: any): void {
    this.cityId = event.target.value;
    this.setDescriptionCity();
  }

  setDescriptionCity(){
    this.cityDescription = this.citiesRequestList[this.cityId-1].cityDescription;
  }

}
